/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smartinteract',
  version: '2.0.12',
  description: 'smart cli interaction'
}
